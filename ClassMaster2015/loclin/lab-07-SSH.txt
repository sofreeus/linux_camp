
Setting up SSH Keys!

From your machine (W1 - workstation)
 $ ssh-keygen
Click Enter for the file name, then...
Enter a password - don't leave this blank...

The following files will be generated in/home/loclin/.ssh/

id_rsa - encrypted key - NEVER SHARE THIS FILE
id_rsa.pub - this is the version that you share - PUBLIC KEY
known_hosts


 $ scp .ssh/id_rsa.pub s1:
 $ ssh s1
On S1...
 $ mkdir .ssh   (OR you can $ ssh localhost)
 $ cat id_rsa.pub > ssh/authorized_keys
Make sure that authorized_keys is not readable by anyone other than me.
 $ chmod 0700 .ssh/authorized_keys



THE EASY WAY....

 $ ssh-copy-id s2

If you wish to remove a key from your known_hosts file do this:
 $ ssh-keygen -R 192.168.2.118


