#!/bin/bash

# Storage Inventory
# Author: Darrin Goodman
# This script will display an inventory of storage devices on this machine.
# Use: ls -lR /dev/disk, lsblk, blkid, fdisk -l, parted -l, df -hT optional: pvdisplay, vgdisplay, tree

clear
echo ""
echo "SCROLL THROUGH THIS SCRIPT ONE PAGE AT A TIME."
echo "USE THE SPACE BAR TO ADVANCE PAGES."
echo "          ((( proceed in 4 seconds... )))"
sleep 4

getInfo() {
echo ""
echo "===> BASIC - MOUNTED VOLUMES (ls /dev/[sh]d?) <==="
## ls /dev/[sh]d? | while read disk; do parted $disk print; done
ls /dev/[sh]d?
echo ""
echo ""
## sleep 3


echo "===> DISPLAYING MOUNTED PARTITIONS (df -hT) <==="
df -hT
echo ""
echo ""
## sleep 3


echo "===> DISPLAYING MOUNTED PARTITIONS (ls -lR /dev/disk) <==="
ls -lR /dev/disk
echo ""
echo ""
## sleep 3


echo "===> LIST BLOCK PARTITIONS (lsblk) <==="
lsblk
echo ""
echo ""
## sleep 3


## echo "===> PARTED (parted -l) <==="
## parted -l
## echo ""
## echo ""
## sleep 3


## echo "===> DISPLAY ATTRIBUTES OF VOLUME GROUPS (vgdisplay) <==="
## vgdisplay
## echo ""
## echo ""
## sleep 3


## echo "===> LIST CONTENTS OF DIRECTORIES IN TREE-LIKE-FORMAT (tree) <==="
## tree /home/loclin/
## echo ""
## echo ""
## ## sleep 3
}

getInfo > info.txt
more info.txt

exit 0;



