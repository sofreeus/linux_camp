Lab: Build S1
-------------

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Please build a server. Call it S1 and for now add it to the hosts file on your workstation.

> It doesn't need much in terms of virtual hardware: 512MB memory and an 8GB hard drive should be fine.

> Make sure you can ssh into it from your workstation using "ssh some-user@s1"

> --Jan

---

You might need to run nmtui, nmcli, or directly modify the ifcfg '-eth' or '-enp' files in /etc/sysconfig/network-scripts/ to make network interfaces start on boot.

--Kim

---

Suggested values:
- Distro: [Fedora](https://www.fedoraproject.org/)
- VM name: S1
- Hostname: S1.(student-project-name).test
- Memory: 512MB
- Storage: 8GB
- Network: (match W1)

Notes:

Set the hostname and reboot.

Verify that you can ssh to the new server from your W1, at least.

```shell
echo '1.2.3.4 s1.alicorn.test s1' | sudo tee -a /etc/hosts
ssh some-user@s1
```

Highly-motivated partners will ensure that they can ssh to one another's servers.

Hints
---

- nmtui
- nmcli
- hostnamectl
- /etc/sysconfig/network-scripts/ifcfg-*
- ip addr show (ip a)
