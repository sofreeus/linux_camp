Lab: Beyond UGO with facls
==========================

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> CC: Sam <sam@macguffins.test>

> Alice/Bob,

> Please make sure Sam can read Eve's home directory.

> Thanks,

> --Jan

---

### Process Notes

This is a job for facls!

### Hints of commands and configs

- getfacl
- setfacl
- man