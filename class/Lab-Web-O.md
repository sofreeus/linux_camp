Lab: (optional) Web matters
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> I'd like to stop paying Marco Marsala Hosting to host our web-site. You guys are plenty smart enough to do this.
> I'd like one of the servers to serve up 'dev' and 'prod' websites. The 'prod' should also come up on just the domain name, and 'www', of course.
>
> Thanks,

> Jan

---

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,
>
> Don't worry so much about the content, if you can get a placeholder website we can have our interns add to it later.
> It's probably easier to get your webserver running first, and then work on configuring your hostnames.
>
> Kim

---

### Notes:

- All the notes that apply to the [“Mail really matters” lab](class/Lab-Mail-O.md) apply equally to this one.
- Add the aliases 'dev.your-project.macguffins.test', 'prod.your-project.macguffins.test', 'www.your-project.macguffins.test', 'secure.your-project.macguffins.test', and 'your-project.macguffins.test' to your webserver in /etc/hosts on all your machines.
- Check out `man curl`

[BasicAuth HOWTO from Apache](http://httpd.apache.org/docs/current/howto/auth.html)

[VHosts with Apache](https://httpd.apache.org/docs/current/vhosts/examples.html)

---

To install a whole software stack, like LAMP:
- `yum groups install lamp-server` in EL
- `sudo zypper install -t pattern lamp_server` in SUSE
- `sudo tasksel install lamp-server` in Debian'ish

<br>
- You may also need to install `apache2-utils`

---
### Basic VirtualHost example

```
<VirtualHost *:80>
    ServerName fully.qualified.domain
    ServerAlias www.fully.qualified.domain
    DocumentRoot /srv/<websitename>
</VirtualHost>
```

Validate VirtualHost with curl:

`curl -H "host: <virtualhostname>" <ip/hostname>`

---
### Prepare your DocumentRoot
- Un-comment the '&lt;Directory /srv/&gt;' block in your Apache configuration at `/etc/apache2/apache2.conf`

---
### Create the dev site:

Configure Apache to serve out the dev site out of `/srv/dev`

- Create an `index.html` file in `/srv/dev` with your content
- Create a config file in `/etc/apache2/sites-available` defining your new virtual host / website
- Enable the new website with `a2ensite`
- Test the configuration: `sudo apachectl configtest`
- Reload the `apache2` service
- Test your configuration using `curl` or web browser. To test from `W1`, you'll need to add an alias in `/etc/hosts`

---
### Create the prod site:

Repeat above for the prod site, this time you will need an alias in your VirtualHost definition.

---
### Create the secure site:

Create a new site separate from prod and dev and secure it for linux admins.

#### Secure it!
- Create a file called `.htaccess` in /srv/secure.

```
AuthType Basic
AuthName "Super Secret Files"
AuthUserFile "/etc/apache2/secure.passwords"
AuthGroupFile "/etc/apache2/secure.groups"
Require group linux_admins
```

- Create your groups file: `sudo vim /etc/apache2/secure.groups`

```
linux_admins: alice bob jan eve
```
- For each user, add them to your password file (authentication is separate from system authentication!) (use -c to create the file for the first user): `sudo htpasswd /etc/apache2/secure.passwords <username>`
- Add the `AllowOverride AuthConfig` to the directory block for /srv in `/etc/apache2/apache2.conf`
- Enable the Apache module to provide authentication: `sudo a2enmod authz_groupfile`
