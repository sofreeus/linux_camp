Lab: Cruddy server incident
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Billy and Jerry reported that your team's file-server / web-server / everything-server is down.

> They found it under the last sysadmin's desk, covered in coffee stains. It won't boot at all, and nobody knows how to log into it.

![Old Server](crasher.jpg)

> Did I mention that THE WEBSITE IS DOWN? I need you to fix that server, and migrate the services off it ASAP.

> Thanks,

> --Jan

---

## Part I

Import 'crasher.ova' to VirtualBox, and do as many of the following as you can:

- [ ] Make it boot-able (re-install grub mbr)
- [ ] Log in (reset the root password)
- [ ] Get the file-server and web-server online
- [ ] Back up the data to the Commons

Hints:

one-page web /var/www/html

files in /shared

samba configuration

## Part II

- [ ] Repair or replace the system.
  Which did you choose and why?
  * Samba share works?
  * Web-site works?
- [ ] web-site files should be shared to W1 by NFS
- [ ] The system should update fully and reboot daily.
- [ ] All users should login with their *own* accounts.
  * Create accounts for alice, bob, and jan
  * Configure them as sudoers
  * Create accounts for dani, fred, gert
- [ ] Web-admins should not have interactive accounts.
  * change preferred shell for dani, fred, gert to /bin/false
  * confirm that dani, fred, gert can NFS and sftp to update web-content, but cannot login interactively

## Bonus Round

Instead of a simple patch and bounce, do this:
  * After max_uptime + $RANDOM%60 minutes
  * verifies no file-system below 20% free
  * patches fully
  * verifies at least two sudoers, then:
    + deletes, disables, or resets root's password
    + deletes root's authorized_keys file
    + deletes any accounts from default_list: ubuntu, admin, linux, builder, osadmin, etc...
  * reboots the box
  * log it all and email a *copy* of the log
