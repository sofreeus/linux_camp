Pre-Lab: Install VirtualBox and connect to the Commons
---

Install VirtualBox on your physical box.

VirtualBox installs and runs best on Linux, but it also installs and runs on Windows and Mac. So, if you currently have Windows or OS X and you don't want to fix it, that's fine, but you might not be able to play the Linux games I might recommend to you. :-)

You can find VirtualBox in the repositories for many Linux distributions, or you can download it from www.virtualbox.org.

Please make sure your default disk location for virtual machine files has LOTS of free space. 16 or 32 GB might be a reasonable minimum.

## Connect to the Commons

The instructor will have built a public NAS for easy file-sharing. Some places in the materials may refer to it as RackMaster or ClassMaster. The same server is meant.

In 2023, that machine is files.sofree.us and there is no need to add a hosts or lmhosts entry.

Connect to Files from your physical host.
- nfs: files.sofree.us:/srv/commons
- smb: (from Windows) \\\\files.sofree.us\\commons
- smb: (from *nix) //files.sofree.us/commons

Example:

```bash
sudo mkdir /mnt/nfs-commons
sudo mkdir /mnt/smb-commons
sudo chattr +i /mnt/*-commons/
sudo mount -t nfs files.sofree.us:/srv/commons /mnt/nfs-commons
sudo mount //files.sofree.us/commons /mnt/smb-commons/ \
  -o guest,vers=1.0,uid=$( id -u ),gid=$( id -g )
```

Ensure that you can read and write to the commons.

1. Create a folder in the Commons named after your MacGuffin, e.g.: alicorn, rage-virus, genesis-device, or whatever... That is your student-project-folder.
* In your student-project-folder, create a folder named `00.Pre-Lab`.
* In the `00.Pre-Lab` folder, put notes that you think may be helpful to your partners and/or other students.
* Finally, create an empty file named `done`.

```bash
mkdir /mnt/nfs-commons/alicorn
mkdir /mnt/smb-commons/alicorn/00.Pre-Lab
touch /mnt/nfs-commons/alicorn/00.Pre-Lab/done
echo 'Done! :^)' > /mnt/smb-commons/alicorn/00.Pre-Lab/done
ls -l /mnt/*-commons/alicorn/00.Pre-Lab/
```

---

Although your virtual network / lab environment will not be fully populated until after the Build W1, S1, and S2 labs ...

... glance at the intended end result for your own lab environment:

![Student Network]( ../student-network.jpg )

... glance at the intended end result for the whole classroom/cabin:

![Class Network]( ../class-network.jpg )
