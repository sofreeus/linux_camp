Lab: NAS Part 1: Better, Faster File-server
===

---------------------------------------------------------------------

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> I'm getting lots of complaints from the art and rendering teams that the central NAS is too slow transferring assets over the WAN, especially for the offices with slow links.

> Can you convert that [document store you created with LVM](class/Lab-LVM-fs.md) to an NFS export?

> Thanks for taking care of this,

> --Jan

---------------------------------------------------------------------

### Process Notes

Consider checking /etc into git. It's a *great* way to roll-back undesired changes, and examine changes made by user-friendly GUI utilities, like YAST and system-config-*

1. Create squashed NFS export
2. NFS mount it and/or autofs and symlink (everywhere)

Mount/remount of file-systems should be completed in maintenance mode (ie: when nobody but root is logged in) and/or when no files are open the affected file-system. Use 'lsof' 'fuser' and 'lslk' to check file-system use.


**NFS Export**

Consider the export options on RackMaster:

```
[sfs@s1-00 ~]$ cat /etc/exports
/srv/commons *(rw,all_squash,no_subtree_check,insecure,anonuid=99,anongid=99)
```

- NFS servers depend on RPC / portmap. If you're hitting a long pause and everything is "right", make sure that RPC / portmap *and* the NFS kernel server are started.
- Enterprise Linux may start a firewall by default. If it does, tune it, or stop and disable it.
- Enterprise Linux enables SELinux by default. You may need to tune it or disable it.
- To check for firewall or selinux problems, turn them off and retry the operation.

1. Install nfs-server
2. Edit /etc/exports
3. start nfs-server and test mount the export on any other machine
4. enable nfs-server to start automatically

**NFS automount**

1. Confirm that autofs is installed and started.
2. Confirm that the line `/net -hosts` in `/etc/auto.master` is active.
3. Symlink from /student-project-name to /net/s1/srv/student-project-name