Linux Camp
==========


Introduction
------------

SFS Linux Camp is a hands-on, pair-oriented "boot camp" for Linux SA's who have some experience, but want to broaden, deepen, and fill the gaps in their knowledge and experience, to become more generally useful, and finally, to prepare for the LFCS exam.

It's our intention to always have this class in a place that allows the student to study and practice very close to nature, and to take breaks in nature. We always bring a frisbee.

Exams: For Linux Camp 2016 onward, students will receive a voucher for the exam at Linux Camp, and will take the exam online, at a time and place of their choosing. We will have before and after evaluation exercises for students to (hopefully) observe that they've learned something during camp.

### About the instructor

    David L. Willson
    RHCE(+Satellite), CCAH, LPIC-1, Novell CLP, LFCS, COA
    Former MCT, MCSE
    25+ years in IT, 15+ years teaching Linux

### About the location

[YMCA Snow Mountain Ranch](http://snowmountainranch.org/)

### Daily schedule

    At your pleasure, rise with the sun, and get in a hike before breakfast.
    Show-and-tell starts at 9AM *sharp* each day
    Lectures alternate with lab exercises until 6PM or so every day.
    After dinner, study or play a game

### Format

Please try to work in small groups of 2 or 3. You'll integrate the new tools and knowledge better when you go through the activities 2 or 3 times, at least once as the operator/novice and at least once as an advisor/expert. You'll learn more as you discuss the details of assignments, and argue about the merits of different approaches. Your focus will be better. You'll have more fun. So, for each assigned task, one of you will take the role of operator and the other(s) will take the role of assistants and/or advisors. In any case, only one of you should be working on the keyboard. The other should be actively assisting: reading the instructions aloud, helping with command syntax, tactics, and strategy, in other words, thinking about the tasks.

    SA = System Administrator
    SE = Systems Engineer

You are playing the roles of new SA's (Alice and Bob) at MacGuffins. We will work from “emails” from the somewhat technically savvy company-owner (Jan). Jan will ask you to do certain things, in certain ways. Jan will only be as specific as necessary to communicate the request. For exercises where you're likely to need a little nudge, you'll have hints from the company SE, Kim. Neither Jan nor Kim will feed you a prescriptive solution. They respect you too much for that. They think you know good answers, and can figure out good solutions to new problems as a team, that you'll ask for clarification or guidance when appropriate.

### Distributions

The Linux Foundation is distribution and vendor neutral, and the exam no longer has a distribution selection or distribution-specific tasks. Good distributions for study are Debian, Fedora, and OpenSuse family.

### Virtualization

We're using Oracle VirtualBox because it's software libre and it allows all the functionality we'll need for this class. The default "system key" for VirtualBox is [right-control]. Tap the system key to release the keyboard and mouse, or hold it to simulate [control]+[alt] for [ctrl]+[alt]+[del], [ctrl]+[alt]+[F1], and so on.

### Strategy and Tips

Script everything. Monkey-script whatever you can't Bash.

Monkey-script? If you at least create a text file listing all the things you clicked and typed as you went along, when you repeat that process, you won't miss important steps. And, if you clean up your list of commands, reducing it to the necessary minimum, you might end up with a good, re-usable "monkey-script", and you'll be one step close to smart-lazy Nirvana.

In the real world, this "script everything" pays off every time you get asked how or whether you did something, and when you do such a good job on something that you get asked to do it again.

You might find this [CheatSheet](CheatSheet.pdf) handy as you're doing labs, or in real life.

### Does Linux matter?

[What is the big deal about Linux, anyway?](BIDD.pdf)

### Sharing

Do you want to do your own class? You are allowed and encouraged to use/study/copy/modify this material as long as you give credit to the author(s), and you allow your students/licensees to do the same. All SFS materials, including the Linux Camp documents, are CC BY SA. See "CC BY SA 3.0" at www.CreativeCommons.org for details.

Before delivering your own class, you should have your Linux+, LPIC-1, LFCS, or RHCSA certification or the knowledge equivalent. You should study and practice the materials ahead of your students. The more you do all those, the more successful your class will be.

I want your patches! If you found a way to make the material better, let me know so both our classes get better!


### Goals

Linux Camp was designed for two purposes, to make junior Linux sysadmins stronger with a battery of hands-on labs that are highly similar to scenarios that might occur on a real network, and by focusing those labs in the domain of a certification, to increase the chance that a given sysadmin will become certified, *without* rote memorization.

Linux Camp targets the practical exam for Linux Foundation Certified Sysadmin (LFCS). Originally, Linux Camp targeted LPI Certification Level 1 (LPIC-1).

The "Big Three" of Linux Camp are:
 - real - Labs should be about solving problems that actually happen, not contrivances.
 - hard - Labs should be satisfyingly hard, but not so hard that learners rage-quit.
 - fun  - Lab stories should be fun to read / listen to, should contain needed information, and should provide chances for students to work together.

I want to help you become creative, helpful sysadmins first, and to help you prepare, through practical experience, for the LFCS exam, second.
