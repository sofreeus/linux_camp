Lab: Samba: Playing nice with Windows
===

******************************

> FROM: StrongJan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> I'm getting lots of complaints from a few Windows users.  They want to access your NAS share, too.  Can you set them with with access via Samba?

> Thanks for taking care of this,

> --StrongJan

******************************

**Samba Share (optional)**

Consider the share definition on RackMaster:

```
[commons]
	comment = Linux Camp / MacGuffins Shared Files
	path = /srv/commons
	read only = No
	guest only = Yes
	guest ok = Yes
```

1. Install Samba
2. Add a share-definition to /etc/samba/smb.conf
3. Add samba accounts for users that will want access over smb/cifs
   Ex: smbpasswd -a bob
3. Start the smb service, test and troubleshoot as necessary
5. Enable the service

**Samba clients**

* In Windows: add S1 to lmhosts and run \\s1\student-project-name
* In GNOME: From Files, click "Connect to Server" and open smb://s1/student-project-name
* You can also use smbclient or mount.cifs

### Hints on commands and configs

- smbstatus
- smbclient
- smbpasswd
- mount.cifs
- /etc/samba/smb.conf
