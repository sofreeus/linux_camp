Lab: Sharing documents
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> I'd like two folders in a /shared directory, 'contractors' and 'linux_admins'. I want you to set them so that the members of the same-named groups can create files and edit each other's documents in those directories, by default.

> I want everyone to be able to read the docs in the 'contractors' directory, but only we Linux Admins should be able to see the things in 'linux_admins' directory.

> Thanks!

> --Jan

---

Notes:

Make a symlink to /net/s1/srv/student-project-name to /shared on your systems.

This will take some testing to confirm you've got it right. Create the directory, set group-ownership and appropriate permissions, making sure to add the setGID permission, so that files are automatically created with appropriate group-ownership. Create some files as various users to test.

On success, only members of the contractors group can create files in the /shared/contractors directory, and other members of the group can edit those files, by default. Everyone can read files in that directory. The linux_admins group-directory is similar to the contractors', except that the owning group is linux_admins, and others *can't* read the directory.

In order to make this sharing work, you might need to modify users' default umask on one or more systems.  It should be 0002.

OpenSUSE stores the system-wide setting in /etc/profile.

Debian's /etc/profile says that umask is set in /etc/login.defs, but that umask setting controls home-folder permissions, not the umask setting for users. Add the command to set the desired umask to /etc/profile and garner joy for all users system-wide.
