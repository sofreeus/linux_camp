Lab: There's exactly one place like home
========================================

*** Rewrite this lab to use autofs home-folders before running it again
*** Do not use this for LFCS

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> It's frustrating to have to copy my files from system to system all time. Would you setup /home as an NFS export/mount? I'd like to have the same home-folder, no matter which system I'm signed onto.

> I think the best way to do it is to add a drive to S2, move /home onto it's own partition, NFS export it, and mount it from the other systems. It should work.

> Thanks for taking care of this,

> --Jan

---

Notes:

This is no trivial ask, but it is pretty routine work. On each system, the work can be completed when nobody (except root) is logged in. root's home-folder is /root, not /home/root, so root may be logged in.

Shut down S2, add a drive, and power it up back up, add a partition to the new drive, put a file-system on the new partition, labeled appropriately, mount it temporarily, move all the home-folders onto it, register it in /etc/fstab appropriately, preferably by UUID, mark the empty old /home directory immutable, mount the new /home file-system, and finally, remove the temporary mount.

Install an NFS server on S2, and export /home.

Be warned! Once you have done this, you need to start the NFS server first and shut it down last, or do some careful mounting and unmounting. NFS clients don't like to lose contact with their server!

If you have time, you absolutely should disable and re-enable the nfs server on s2 using update-rc.d and comfirm the desired effect.

---

On each of the other systems, move /home to /tmp/old-home, create a new, empty /home directory, mark it immutable, register the NFS mount in /etc/fstab, and mount it.

On some systems, you'll need to install things, and start services. When that happens, remember to use chkconfig or update-rc.d to enable the services to start on boot, ie: to "enable" them.

Like 'yum provides' helps you find missing commands on enterprise, so 'apt-file search' can help on debian.

Be sure to test changes to /etc/fstab with 'mount -a' or 'mount /mount-point' *before* you reboot the box (or someone else does).
