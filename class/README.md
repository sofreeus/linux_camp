Linux Sysadmin Night School 2020 Plan
===

[Introductions](Intro.md)

[Lab-Build-W1](Lab-Build-W1.md)

[Lab-Build-S1](Lab-Build-S1.md)

[Lab-Build-S2](Lab-Build-S2.md)

*** Optional: [Lab-Recover](Lab-Recover.md) Part I ***

[Lab-Users](Lab-Users.md)

[Lab-Root](Lab-Root.md)

[Lab-Sudoer](Lab-Sudoer.md)

[Lab-SSH](Lab-SSH.md)

[Lab-Network](Lab-Network.md)

[Lab-Root.md](Lab-Root.md)

[Lab-HardwareInventory](Lab-HardwareInventory.md)

[Lab-Maintenance](Lab-Maintenance.md)

[Lab-LVM-fs](Lab-LVM-fs.md)

[Lab-NAS-I](Lab-NAS-I.md)

[Lab-NAS-II](Lab-NAS-II.md)

[Lab-Doc-Share](Lab-Doc-Share.md)

[Lab-Later](Lab-Later.md)

[Lab-EmployeeHandbook](Lab-EmployeeHandbook.md)

[Lab-Mail](Lab-Mail.md)

[Lab-Web-O](Lab-Web-O.md)

[Lab-Scripts](Lab-Scripts.md)

[Lab-Schedule](Lab-Schedule.md)

[Lab-Evil-Eve](Lab-Evil-Eve.md)

[Lab-GUI](Lab-GUI.md)

[Lab-NTP](Lab-NTP.md)

[Lab-StorageInventory](Lab-StorageInventory.md)

[Lab-cron-n-rsync](Lab-cron-n-rsync.md)

As Time Allows
---

[Lab-Print](Lab-Print.md)

[Lab-Mail-O](Lab-Mail-O.md)

[Lab-Home](Lab-Home.md)
Kill this exercise! Don't ever do this, even if you have time! It is a Bad Idea!

[Lab-Wordpress-O](Lab-Wordpress-O.md)

[Lab-cdir-O](Lab-cdir-O.md)

[Lab-Alien-O](Lab-Alien-O.md)

[Lab-web-db-O](Lab-web-db-O.md)

[Lab-Shell-O](Lab-Shell-O.md)
