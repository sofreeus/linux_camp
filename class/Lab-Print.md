Lab: It's sure nice to be able to 'print' PDF's
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Could you setup one of the servers with a print-queue that drops PDF's? Ideally, I'd like it to drop in the user's home folder, but we can work on that later.

> --Jan

---

Notes:

This turns out to be pretty easy, if you use the cups-pdf package.

If time and equipment allow, setup a queue to a real printer.

If time allows, connect to the print-queue(s) from the other boxes.
