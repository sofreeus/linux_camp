Lab: Get your scripts together!
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob:

> Gah! Why isn't 'ifconfig' on my path? Would you set the systems so that the path for linux_admins includes the sbin's and the scripts directory in /shared/linux_admins? Oh, and create a scripts directory in the linux_admins directory and move the final version of each of the scripts we've written so far into it.

> Theoretically, we'll be able to run things directly, without specifying the full path, from any machine on the network. Won't that be nice?

> Thanks again...

> -- Jan, the Frustrated One

---

Notes:

To add the sbin directories and the scripts directory to the current path, you might use a command like the one below:

    export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin:/shared/linux_admins/scripts

One solution was to loop through the members of linux_administrators and append the above line to their .bashrc file.

A better way would check for each path-element before adding it to the PATH.

Maybe you can find a way to do this automatically for members of 'linux_admins', so that changes in group-membership have an effect on the PATH.
