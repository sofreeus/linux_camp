Lab: The unknown root password
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Someone (Eve?) changed the root password on S1. Can you reboot the box to single user mode and reset the root password?
> --Jan

---

### Process Notes:

On S1, run these commands to set root's password to an unknown value and reboot:

```shell
newpw=$( uuidgen )
echo -e "$newpw" | sudo passwd --stdin root
reboot
```

[Instructions for RedHat (CentOS) 7](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/sec-Terminal_Menu_Editing_During_Boot.html#sec-Changing_and_Resetting_the_Root_Password)