Lab: Performing one-time tasks “later”.
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Can you make sure to reboot all the servers at 2:30 tomorrow morning?

> Thanks,

> --Jan

---

Notes:

Use 'at' so you don't have to stay up. Set a long delay and a warning message on the reboot. Make sure it goes off correctly by checking uptime after the reboot.
