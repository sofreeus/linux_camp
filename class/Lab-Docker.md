Lab: Containerize all the things!
=================================

---

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Guys,

> We are going to blow Jan's doors off.

> We hosted in VPS. We hosted on our own machines. We virtualized our
> machines. Now, we're going *plaid*! (SpaceBalls)

> Containers get us:
> * immutabe infrastructure
> * hundreds of instances per host
> * simple versioning

> Let's start with a simple Apache httpd with the web-page built in.

> Later, we'll do great things with LBs to get high service uptimes.

> Make sure to map host port 8080, 8000, or 8888 to the container
> and set it to run automatically when the system starts.

> Kim

---

```bash
docker search apache
```

https://hub.docker.com/_/httpd

---

Create an index.html

```html
<html>
    <head><title>Bear Metal!</title></head>
    <body>
        <h1>Bear Metal!</h1>
        <p>&#128059; &#129304;</p>
    </body>
</html>
```

Create a Dockerfile to build a server for it

```Dockerfile
FROM httpd:2.4
copy index.html /usr/local/apache2/htdocs/
```

Build it and run it

```bash
docker build -t my-apache2 .

docker run --name my-node --detach --restart=unless-stopped -p 8080:80 my-apache2
```

Test it

```bash
curl localhost:8080
xdg-open http://localhost:8080
```

Stop it, fiddle with it, re-build it, and re-run it

```bash
docker rm -f my-node
vim index.html
vim Dockerfile
docker pull httpd:2.4
docker build -t my-apache2 .
docker run --name my-node --detach --restart=unless-stopped -p 8080:80 my-apache2
```

Finally, reboot and ensure that it comes up.


Examples of other useful Docker expressions:

```bash
docker ps
docker ps -a
docker images
docker cp nutty_professor:/var/log/messages nutty_message_log
docker exec -it nutty_professor /bin/bash
```