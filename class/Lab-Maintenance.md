Lab - Maintenance
===

> FROM: Kim <kim@macguffins.test>

> TO: SysAdmins DL <sysadmins@macguffins.test>

> Teams,

> Happy Friday! Sorry it's maintenance night but I think you'll dig what we put in the taps. Go easy; you know what they say about drunken system-administration...

> You know the drill, but here it is anyway:

> - *DON'T* start before the maintenance window opens at 5:30pm!
> - Check disk space and add storage to any filesystems more than 80% used.
> - Update all the things. Look at the Rosetta Chart if you don't remember how.
> - Reboot
> - Check and fix services as needed.

> If I don't get calls from your users, or Jan, you *win*!

> --Kim

---

Real world: Don't start a massive patch until pretty late, so you're not clogging the pipe. We only have the one pipe.

The [Rosetta Chart](../chart-rosetta.md) might be useful.
