# Sysadmin Sunday School

## Session 0

What is a Sysadmin?

What do sysadmins do? How much do sysadmins make?
- troubleshoot problems with server machines
- make good money
- add/remove
    * users
    * sofware
    * services
- eat donuts

What is the [LFCS Certification](https://training.linuxfoundation.org/certification/linux-foundation-certified-sysadmin-lfcs/)?
- Practicum
- Intermediate

Participant Introductions

1. Connect to [GitLab.com](www.gitlab.com/sofreeus)
2. Clone the training materials:
    ```bash
    git clone git@gitlab.com:sofreeus/Linux-Camp.git
    # or
    git clone https://gitlab.com/sofreeus/Linux-Camp.git
    ```
3. Connect to the chat channel [SFS' Mattermost](mattermost.sofree.us/sfs303/channels/linuxcamp23)

How shall we study?

- 3 in VirtualBox:

    One or more graphical workstations and two or more text-mode server machines.

    Three different distros is recommended, but not required.

    Here are some suggestions:

    * Workstation (W1) [OpenSuse](https://get.opensuse.org/tumbleweed/?type=desktop#download) or Slackware or MX Linux or Bodhi or maybe a nice Arch-derivative like Manjaro
    * Server (S1) [Fedora](https://www.fedoraproject.org/server/download/)
    * Server (S2) [Debian](https://www.debian.org/download)

- Reserve 2-4 hours for study between sessions. Yes, it takes that long to learn.

You're ready for Session 1 when you have 3 in VirtualBox.

- Send your GitLab username to [DLW](https://mattermost.sofree.us/sfs303/messages/@dlwillson)

## Session 1

Checkpoint:
- Mattermost
- Materials
- Machines

pairs, mobs, or ensembles

two things

[Story](story.md)

[Rosetta Chart](chart-rosetta.md)

[Class](class/README.md)

- Intro
- Pre-Lab
- Lab00: S1
- Lab01: S2
- Lab02: S3

## Session 2

Pre-Lab (i.e. connect *everything* to the Commons)

autofs?

ssh basics
- ssh with a password
- ssh-keygen
- ssh-copy-id
- ssh with a key
- $( ssh-agent ) && ssh-add

Handful of help

## Session 3

Review Handful of Help

Shell-scripting Fundamentals

Add and Remove:
- Users (alice,bob,charli,dani,eve,fred,gert)
- Groups (contractors, linux_admins)
- Packages (ncdu)
- Filesystems (commons)

Labs:
- Lab-Commons
- Lab-Users

## Session 3.5

Labs:
- Lab-Root (skipped)
- Lab-Sudoers
- Lab-SSH (done previously)

## Session 4 (2023-06-25)

Checkpoint:
- handful of help is memorized
- can ssh from workstation to all server machines
- commons is always within reach
- commons has a folder for you
- your commons folder has your user-creation-script
- for each of your machines:
    * machine can reach the commons
    * users have been created
    * groups have been created
    * contractors have expired
    * alice and bob are sudoers
    * at least one sudoer has a password you know
- all your machines are totally patched up

We will not doing these labs as a group, but you may do them on your own and I will gladly support you.

- Lab-Root
- Lab-SSH
- Lab-Network
- Lab-The-Sam-Hammer
- Lab-HardwareInventory

Tonight, together, on the machines of someone who is caught up, we will do: Lab-LVM-fs and Lab-NAS-I

That should hit these things:

User-Private Group scheme: chown, chmod, setfacl, getfacl

Service Management: systemctl start, status, enable, stop, disable (discover opened files and ports)

If there's time left, we might:

Lab Web-O

ssh and http listening ports

## Session 5 (2023-07-09)

[Limits](class/Lab-Limits.md)

cron tar rsync backups
- [Lab Rsync](class/Lab-cron-n-rsync.md)
- [Lab Schedule](class/Lab-Schedule.md)
- [Lab Later](class/Lab-Later.md)

## Session 6 (2023-07-16)

[Software RAID with mdadm](class/Lab-RAID.md)

[More than UGO with facls](class/Lab-facl.md)

[Filesystem repair with badblocks and fsck](class/Lab-fsck.md)

## Session 7

virtd, virsh, etc.

## Session 8

containerd, docker, etc.

Leftovers

Review and Backfill
- ip addr show / ip route
- ss -plant / ss -pl
- links: hard and symbolic
- review redirection: <, >, 2>, 2>&1, |, >>, | tee, | sudo tee
- package which owns a file or would provide a file
- awk, cut, grep, sed, diff (recursive)
