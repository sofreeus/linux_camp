How to do a lab
===

Go and sit close to your parter.

Generally, one partner should operate the console, while the other partner:
- Takes notes (and pictures and drawings)
- Helps with research on hard questions
- Oversees, advises, questions, and encourages

Share the screen. Lean in, or if you're blind, or your partner is stinky, use a secondary monitor, vnc, screen, or something. Advising partners should be able to easily see what the operating partner is doing.

1. Create a student-project-folder in the Commons if you haven't already.
*  Create a new folder in your student-project-folder for each lab.
*  Put a copy of *everything* related to the lab in the lab-folder: ideas, scripts, data-files, notes, screenshots, pictures of hand-written notes, inspirational sayings, insights, and reminders of things you want to research later.
*  When your team has completed the lab and you are ready for the next lecture, raise both hands above your head and wave them around and shout, "Zagami!" or "Hakuna Matata!" or "Banzai!" But keep it short, others are still working.
*  Also, place an empty file named 'done' in the lab-folder.

definition-of-done:

* [ ] Our solution meets the expressed requirements.
* [ ] I understand why we did what we did.
* [ ] I wrote some notes.
* [ ] I could explain our solution / make meaningful observations.
* [ ] We placed 'done' files in our lab-folders.
* [ ] I am fueled and ready to learn moar stuph.
