- [x] Revise W1, S1, S2 Labs w correct distro names and links!
- [x] Add files and files.int to Gandi
- [x] Add Samba to files - Update the docs
- [x] Copy /etc/exports and smb.conf to /TeacherTools
- [x] Rehearse connecting to the Commons... desktop file browser, autofs, and smbclient are all good for temp anon

Sunday Prep
-----------
- [x] Find and review FFHP
- [x] Rectify plan to LFCS
- [x] Link 3 good solutions to Users lab
- [x] Archive big folders (JRohrer stuphs)
- [x] Revise big maintenance patch lab notes!

- Find or write labs for:
    * limits
    * filesystem repair
    * mdadm
    * virsh
    * docker

- ? Add AnonFTP to files?
- ? Add an HTTP share to files?
- ? Revise Rosetta Chart ( add Arch / pacman )
