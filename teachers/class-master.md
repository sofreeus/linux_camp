## How To Build ClassMaster

ClassMaster 2023 Build Notes
- exports and smb.conf are in ClassMaster2019
  Move them here.
  Write a script or an Ansible playbook to do the setup
- Machine is 2 cores processor, 4 G memory, 32 G and 128 G storage, 2 NICs
- Share Commons over NFS, SMB, FTP, all squash, anon OK
- Share User folders over SMB, NFS, and FTP, no squash, no anon
- Can we get Group folders? I dunno.

debian

```
ssh osadmin@192.168.73.107
su -
apt install sudo
```

---

```
cfdisk /dev/sdb
mkfs.ext4 -L Commons /dev/sdb1
blkid /dev/sdb1
# /dev/sdb1: LABEL="Commons" UUID="f0b0ac0c-9a43-4dab-8e1f-08e847401c8f"
mkdir /srv/commons
chattr +i /srv/commons
echo 'UUID="f0b0ac0c-9a43-4dab-8e1f-08e847401c8f" /srv/commons auto defaults 1 2' >> /etc/fstab
mount /srv/commons/
chown nobody. /srv/commons/
chmod 7777 /srv/commons/
```

---

```
echo 'allow-hotplug ens19
iface ens19 inet static
  address 67.42.246.118
  netmask 255.255.255.240
  gateway 67.42.246.126
  dns-nameserver 8.8.8.8
' >> /etc/network/interfaces
vim.tiny /etc/network/interfaces
reboot
```

---

```
apt update && apt -y dist-upgrade && apt -y autoremove

apt install apt-file
apt-file update

apt-file search exportfs
apt install nfs-kernel-server
echo '/srv/commons *(rw,all_squash,no_subtree_check,insecure,anonuid=65534,anongid=65534)' > /etc/exports
systemctl restart nfs-server.service
```

---

```
apt-file search smbstatus
sudo apt install samba
cat /srv/commons/smb.conf > /etc/samba/smb.conf
vim.tiny /etc/samba/smb.conf
systemctl restart smbd.service
```

---
