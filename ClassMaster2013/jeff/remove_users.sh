#!/bin/bash -eu
#Jeff White
#Remove users and group
users="bob alice charlie dani eve fred jan"
groups="contractors linux_admins"

for group_name in $groups; do
	groupdel $group_name
done

for user_name in $users; do
	userdel -r $user_name
	groupdel $user_name
done
