#Create users from a list

#!/bin/bash -eu
PATH+=:/sbin:/usr/sbin:/usr/local/sbin

users="Jan Charlie Dani Eve Fred Gert"
groups="Contractors Linux_admins"
contractors="Dani Eve Fred"
admins="Jan Eve"

#Create user accounts for Jan, Charlie, Dani, Eve, Fred, Gert

for user in $users
do 
	sudo useradd $user
	echo -e "Freedom-16\nFreedom-16" | sudo passwd $user
done

#Create user groups

for group in $groups 
do
	groupadd $group
done

for user in $contractors
do 
	usermod -aG "Contractors" $user
done

for user in $admins
do
	usermod -aG "Linux_admins" $user
done

#Expire contractors in one day
for user in $contractors
do
	chage -E 2016-08-22 $user
done

#Check email
