Physical Disk: sda
Partician Type: 
---File System Type---
/dev/sda1: Linux
/dev/sda2: 
/dev/sda5: LVM
 
---Partician Size---
sda1: 243M
sda2: 1K
sda5: 7.8G
 
---Percent Used---
/dev/sda1: 33%
 
---Partician UUID---
/dev/sda1: UUID="041ec21e-edc7-4569-a4f4-81df567fbb02"
/dev/sda5: UUID="eQTfUX-SL70-25FM-KhKo-4KQr-Gvtt-TrX2VE"
 
