#!/bin/bash -eu

PATH+=:/usr/local/sbin:/usr/sbin:/sbin

#Report all physical disks
echo -n "Physical Disk: "
echo $( lsblk | grep disk | awk '{ print $1 }' )

#Whats the paritican table
echo -n "Partician Type:"
sudo fdisk -l /dev/sda | grep "Disk label type" | cut -f 2 -d :

echo " "

#File system type
echo "---File System Type---"
sudo fdisk -l /dev/sda | grep [sh]d[[:alpha:]][[:digit:]] | awk '{ print $1 ": " $7 }'

echo " "

#size fo the partician
echo "---Partician Size---"
lsblk | grep part | tr -d ├─└─ | awk '{ print $1 ": " $4 }'

echo " "

#Percent used
echo "---Percent Used---"
df -hT | grep "sda" | awk '{ print $1 ": " $6 }'

echo " "

#UUID
echo "---Partician UUID---"
blkid | grep "sda" | awk '{ print $1 " " $2 }'

echo " "

