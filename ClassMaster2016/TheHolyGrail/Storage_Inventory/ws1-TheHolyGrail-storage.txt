Physical Disk: sda
Partician Type: dos
 
---File System Type---
/dev/sda1: Linux
/dev/sda2: LVM
 
---Partician Size---
sda1: 399M
sda2: 15.6G
 
---Percent Used---
/dev/sda1: 26%
 
---Partician UUID---
/dev/sda1: UUID="92f3e21f-0659-459a-8556-2d4c91027905"
/dev/sda2: UUID="oGFq8H-4sTc-7p4G-g7cU-eu0M-pJdp-0cmzBl"
 
