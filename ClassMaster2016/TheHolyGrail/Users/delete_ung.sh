#! /bin/bash -eu
PATH+=:/sbin:/usr/sbin:/usr/local/sbin

#Deletes users
users="bob alice jan kim sam charlie dani eve fred gert"

for user in $users
do
	sudo userdel -r $user
done 

#Deletes groups
sudo groupdel contractors

sudo groupdel linux_admins

