#!/bin/bash -eu

cd /home
for user in *
do
	if ! [[ $user == 'lost+found' || $user == 'linux~' ]]
	then
		if [[ $( ls $user/ | grep Desktop ) == 'Desktop' ]]
		then
			echo "Adding Handbook to user $user"
			sudo cp /commons/employee_handbook.pdf $user/Desktop/
		else	
			echo "No Desktop. Creating one for user $user"
			sudo mkdir $user/Desktop
			sudo cp /commons/employee_handbook.pdf /home/$user/Desktop/
			sudo chown --recursive --reference $user/public_html $user/Desktop/
		fi
	fi
done



