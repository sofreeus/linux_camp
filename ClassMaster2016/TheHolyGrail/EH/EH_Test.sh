#!/bin/bash -eu

cd /home
for user in *
do
	if ! [[ $user == 'lost+found' || $user == 'linux~' ]]
	then
		if [[ $( ls $user/ | grep Desktop ) == 'Desktop' ]]
		then
			echo "Adding Handbook to user $user"
		else	
			echo "No Desktop. Creating one for user $user"
		fi
	fi
done



