#!/bin/bash -eu

PATH+=PATH:/user/local/sbin:/usr/sbin:/sbin

#Report all physical disks
echo " "
echo -n "Physical Disk: "
lsblk | grep disk | awk '{ print $1 }' 
#whats the paritican table
echo -n "Partition Table:"
sudo fdisk -l /dev/sda | grep "Disk label type" | cut -f 2 -d :
#file system type
echo " "
echo  "---File System Type---"
sudo fdisk -l /dev/sda | grep [sh]d[[:alpha:]][[:digit:]] | awk '{ print $1  " : " $7 }'
echo " "
#size of the partician
echo "---Partition Size---"
lsblk | grep part | tr -d '├─└'| awk '{ print $1  " : " $4 }'
#percent used
echo " "
echo "---Percent Used---"
sudo df -hT | grep 'sda' | awk '{ print $1 " : " $6 }'
#label
#serial number
echo " "
#UUID
echo "---Partition UUID---"
blkid | grep sda | awk '{ print $1 "  " $2 }'
echo "  "
