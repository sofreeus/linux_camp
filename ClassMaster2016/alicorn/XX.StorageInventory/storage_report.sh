#!/bin/bash -eux
PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin

for disk in /dev/[sh]d[[:alpha:]]
do
  echo -n "$disk: "
  sudo fdisk -l $disk |
    grep "Disk label type"
done

for part in /dev/[sh]d[[:alpha:]][[:digit:]]*
do
  sudo blkid $part
done

df -hT | grep -v tmpfs
