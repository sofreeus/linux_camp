#!/bin/bash -eu
echo "**** Host Info ****"
hostname
uname -sv
echo "**** CPU / Memory ****"
cat /proc/cpuinfo | grep "model name"
cat /proc/meminfo | grep "MemTotal"
echo "**** DMA Info ****"
cat /proc/dma
echo "**** Interrupts Info ****"
cat /proc/interrupts
echo "**** Storage Info ****"
lsblk
echo "**** Network Info ****"
lspci | grep "Ethernet"
echo "**** Network Interface Info ***"
ifconfig | grep -E "addr|Link"
echo "**** VGA Info ****"
lspci | grep "VGA"
