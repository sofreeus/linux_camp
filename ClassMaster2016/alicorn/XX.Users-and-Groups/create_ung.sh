#!/bin/bash -eu
PATH+=:/sbin:/usr/sbin:/usr/local/sbin

# make sure that they all have accounts on all the machines under your control and that their UID's and GID's are consistent across machines.

# Please create user-accounts for yourselves, Jan, Kim, Sam, Charlie, Dani, Eve, Fred, and Gert.

for user in alice bob charlie dani eve fred gert jan kim sam
do
  sudo useradd $user
  echo -e "Freedom-16\nFreedom-16" | sudo passwd $user
done

# We also need two groups: 'contractors' should have Dani, Eve, and Fred, and 'linux_admins' should have you two, me, and Eve.

sudo groupadd contractors
for user in dani eve fred
do
  sudo usermod -aG contractors $user
done

sudo groupadd linux_admins
for user in alice bob jan eve
do
  sudo usermod -aG linux_admins $user
done

# Make sure all the contractors' accounts expire in six months.

for contractor in $( getent group contractors | cut -f 4 -d : | tr ',' ' ' )
do
  sudo usermod -e 2016-08-21 $contractor
done

# After your account is on W1, please make sure you can easily send and receive email.
