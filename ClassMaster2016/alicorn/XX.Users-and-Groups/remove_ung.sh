#!/bin/bash -eu
PATH+=:/sbin:/usr/sbin:/usr/local/sbin

# make sure that they all have accounts on all the machines under your control and that their UID's and GID's are consistent across machines.

# Please create user-accounts for yourselves, me, Charlie, Dani, Eve, Fred, and Gert.

for user in alice bob charlie dani eve fred gert jan kim sam
do
  sudo userdel -r $user
done

# We also need two groups: 'contractors' should have Dani, Eve, and Fred, and 'linux_admins' should have you two, me, and Eve.

sudo groupdel contractors
sudo groupdel linux_admins

# Make sure all the contractors' accounts expire in six months.

# After your account is on W1, please make sure you can easily send and receive email.
