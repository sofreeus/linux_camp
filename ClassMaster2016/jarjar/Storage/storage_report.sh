#!/bin/bash

for DISK in /dev/[sh]d[[:alpha:]]
do
	echo $DISK
	sudo fdisk -l $DISK | grep "Disk label type"
done

for PARTITION in /dev/[sh]d[[:alpha:]][[:digit:]]*
do
	sudo blkid $PARTITION
done

df -hT | grep -v tmpfs
