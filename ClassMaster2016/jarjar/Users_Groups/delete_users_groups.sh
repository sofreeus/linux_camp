#!/bin/bash
# Author Justin Lang <justinslang88@gmail.com>
# Script to create users and put them in groups for Linux Camp 2016

users="jan alice bob kim sam charlie dani eve fred gert"
groups="contractors linux_admins"
contractors="dani eve fred"
admins="jan alice bob eve"

for user in $users
do
	sudo userdel -r $user
	rc=$?
	if [ $rc -ne 0 ]; then
		echo "I dun fucked up deleting users"
	exit 1
	fi
done

for group in $groups
do
	sudo groupdel $group
	rc=$?
	if [ $rc -ne 0 ]; then
		echo "I dun fucked up deleting groups"
	exit 1
	fi
done

