#!/bin/bash -eu

# Matt James

# This drops off a copy of the empoyee handbook whenever a user logs in.

show_icons=$(gsettings get org.gnome.desktop.background show-desktop-icons)
[[ $show_icons == 'true' ]] || gsettings set org.gnome.desktop.background show-desktop-icons true 

[[ -d ~/Desktop ]] || mkdir -p ~/Desktop
[[ -e ~/Desktop/employee_handbook.pdf ]] || ln -s /commons/employee_handbook.pdf ~/Desktop
