#!/bin/bash -eu
# Updated by Matt James 8-21-2016
# Matt James and Gary Romero (papagarheade) 2013

PATH+=:/sbin:/usr/sbin:/usr/local/sbin

[ $(whoami) != 'root' ] && sudo -- "$0" "$&"

#processor info
echo '--- Processor Speeds ---'
grep -E 'cpu MHz|bogomips' /proc/cpuinfo | sort | uniq -c
echo $( grep processor /proc/cpuinfo | wc -l) Total \"Processors\"
echo ""

#memory info
echo '--- Total Memory ---'
grep MemTotal /proc/meminfo
echo ""

#storage info
echo '--- Hard Disk Info ---'
lsblk
echo ""

#network info
echo '--- Network Info ---'
lspci -nn | grep -E 'Ethernet|Wireless'
echo ""

#video info
echo '--- Video Card Info ---'
lspci -nn | grep -E 'VGA'
echo ""

#sound card info
echo '--- Sound Card Info ---'
lspci -nn | grep -E 'Audio'
echo ""

#irq / dma info
echo '--- Interrupts ---'
cat /proc/interrupts
echo '--- DMA Channels ---'
cat /proc/dma
echo ""

#Kernel info
echo '--- Dist / Kernel info ---'
hostnamectl status | grep Operating | cut -f2 -d:
hostnamectl status | grep Kernel | cut -f2 -d:

echo ""

#System condition info
echo '--- System Condition ---'
echo -e "\e[31mWhat is the condition of this machine?\e[00m">&2
read condition
case $condition in
*ood)
	echo "System condition is good"
	;;
*ad)
	echo "System condition is bad"
	;;
*gly)
	echo "System condition is ugly"
	;;
*)
	echo "No system condition recieved" && echo $condition
	;;
esac
echo ""

#System warning messages
echo '--- Boot time Warnings and Errors ---'
grep fedora /etc/*-release>/dev/null && journalctl -b | grep -Ei 'warning|error'>&2 || cat /var/log/dmesg | grep -Ei 'warning|error'>&2
grep fedora /etc/*-release>/dev/null && journalctl -b | grep -Ei 'warning|error' || cat /var/log/dmesg | grep -Ei 'warning|error'
echo -e "\e[31mDoes anything look alarming / concerning to you?\e[00m">&2
read concerns
echo 'Were there any concerns? ' $concerns
echo ""

