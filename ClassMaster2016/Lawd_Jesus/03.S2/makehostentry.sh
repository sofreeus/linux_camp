#!/bin/bash -eu
#
# Matt James - 8-20-2016
#
# This script grabs the IP address, hostname, and alias 
# and creates a valid host name for that host.  
# (So that sudo doesn't bitch about not being able to 
# resolve it's own hostname)

ipv4=$(ip -4 a show eth0 | grep -E 'inet.*global' | awk '{ print $2 }' | cut -f 1 -d / )

# or you can just do this...
ip=$(ip a | grep 192.*/ | tr /)

fqdn=$(hostnamectl | grep Pretty | cut -f 2 -d : )
alias=$(hostname -s)

# add entry to hosts file
echo $ipv4 $fqdn $alias | sudo tee -a /etc/hosts 


