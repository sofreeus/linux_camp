#! /bin/bash -el

#This script places the Hnadbook on every users desktop.
#Place this scrip in /etc/profile.d to run on every user login.


user=$(whoami)

#Copy Handbook to Desktop
cp /commons/employee_handbook.pdf /home/$user/Desktop || :
#Keep naughty users from deleting the Handbook and activate Dekstop icons
chmod 444 /home/$user/Desktop/employee_handbook.pdf &&
gsettings set org.gnome.desktop.background show-desktop-icons true
