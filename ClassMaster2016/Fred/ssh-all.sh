#!/bin/bash -eu

# Auto login to all the things

# ssh -i /home/thowd/Documents/KEYS/id_rsa  root@mail.howd.tech
# ssh -i /home/thowd/Documents/KEYS/id_rsa  root@www.howdtech.com

PS3='Please enter your choice: '
options=("Mail Server" "Owncloud Server" "Windows-Utility" "Linux-WS-CLI" "Linux-WS-GUI" "Quit")
select opt in "${options[@]}"
  do
      case $opt in
          Mail*)
              ssh -i /home/thowd/Documents/KEYS/id_rsa  root@mail.howd.tech
              ;;
          own*)
              ssh -i /home/thowd/Documents/KEYS/id_rsa  root@www.howdtech.com
              ;;
          Quit)
              exit 0
              ;;
          Windows-Utility)
              xfreerdp -u thowd -d gpssource.local -g 1366x701 10.1.1.75
              ;;
          Linux-WS-CLI)
              ssh admin2@10.1.6.97
              ;;
          Linux-WS-GUI)
              xdg-open /home/thowd/Desktop/Workstation
              ;;
          *) echo invalid option;;
      esac
  done
