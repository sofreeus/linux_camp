#!/bin/bash -eu

PATH=$PATH:/sbin:/usr/sbin

[ "$(whoami)" != "root" ] && exec sudo -- "$0" "$@"

lsblk | grep -v lvm

parted -l

df -h | grep -v tmpfs
