# Windows 10 setup for SFS Linux camp Prelab

Edit in administrator mode:
	C:\Windows\System32\drivers\etc\hosts

Add the line:
```192.168.134.1 ClassMaster.SFS.test ClassMaster```
	
Save

Open windows explorer and enter
```\\classmaster\commons```

might take a while to come up.  You'll be prompted for username & password: ```sfs/Freedom-16. Domain name is ignored.```

Other notes:

* pcmanfm is a good file manager to use on linux
* to use nfs, install nfs-common. This will make command line much easier.