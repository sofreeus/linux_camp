#!/bin/bash -eu
# Add user handbook to each user home directory
# run as sudo

handbook="/commons/employee_handbook.pdf"
cd /home
for user in *
do
  if ! [[ $user == 'lost+found' ]]
  then
    if [ ! -d "$user/Desktop" ]; then
      mkdir /home/$user/Desktop
    fi
    ln -sf $handbook $user/Desktop/
    if [ -d "$user/public_html" ]; then
      chown --recursive --reference $user/public_html $user/Desktop/
    else
      chown $user:$user $user/Desktop/
    fi
  fi
done

#from travis to show icon on desktop, except this needs to go in each 
# user's profile.d
gsettings set org.gnome.desktop.background show-desktop-icons true
