#!/bin/bash -eux
# invoke script with root priviledge, maybe
#Anyway, make sure you both have your own accounts on all the machines, and membership in a group that's visible everywhere. I think you have 'linux_admins' everywhere and your UID's are in sync, right? If so, just add 'linux_admins' to sudoers. I'll leave it up to you whether or not to make it password-less, but I want you to be able to sudo everything everywhere.

#Now is a great time to set a good password on the 'alice' or 'bob' account on w1 and start using it for everything. For an extra challenge, remove all the other passwords or replace them with unknown values. To see who has passwords, view /etc/shadow as root.
# visudo
# /etc/sudoers
# /etc/sudoers.d/
# sudo -l -U gert
# AFTER YOU ADD ANOTHER SUDOER: Normalize sudo on the OpenSUSE machine by removing the Defaults targetpw and  ALL ALL=(ALL) ALL lines from the sudoers file (using sudo visudo).


# In a previous lab, we defined a group "linux_admins". Simply add a file to the 
# sudoers.d directory of the same name with contents as defined.

echo '%linux_admins ALL=(ALL) ALL' |
  sudo tee -a /etc/sudoers.d/linux_admins

# same as above with no-password option
#echo '%linux_admins ALL=(ALL) NOPASSWD: ALL' | tee -a /etc/sudoers.d/linux_admins

# AFTER adding linux admins to suders.d, on openSUSE, use visudo to edit /etc/sudoers
#  to remove lines Defaults targetpw and  ALL ALL=(ALL) ALL lines
# Just execute the command /usr/sbin/visudo; it automatically opens the sudoers file 

