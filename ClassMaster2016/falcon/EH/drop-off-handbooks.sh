# Lab: Employee Handbook
# ======================
# 
# > FROM: Jan <jan@macguffins.test>
# 
# > TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>
# 
# > Please put a copy of the employee handbook on everyone's desktop. Apparently, some of our contractors don't know we have a dress-code that doesn't include pink sweats and bunnie slippers.
# 
# > I had this done last year, too, but none of the new employees have it. This year, please make sure it sticks.
# 
# > TY,
# 
# > --Jan
# 
# ---
# 
# Create symlinks on all the current users' desktops with a simple loop.

cd /home
for i in *; do
  id $i &> /dev/null || continue
  mkdir -p $i/Desktop
  ln -sf /commons/employee_handbook.pdf $i/Desktop/
  chown -R --reference=$i/public_html $i/Desktop 2>/dev/null || chown -R $i:$i $i/Desktop
done

# 
# For new users, you'll need to modify the home-folder template. You'll probably need to create the Desktop directory in /etc/skel/. Then, create the sym-link in /etc/skel/Desktop, and create a new user to test.
#

mkdir -p /etc/skel/Desktop
ln -sf /commons/employee_handbook.pdf $_

password=Freedom-16
if ! id whf &> /dev/null; then
  useradd whf
  printf "$password\n$password\n" | sudo passwd $user
fi
