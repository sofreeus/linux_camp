#!/bin/bash -eux
PATH+=:/sbin:/user/sbin:/usr/local/sbin

# Lab: Create users and groups
# ============================
# 
# ---
# 
# > FROM: Jan <jan@macguffins.test>
# 
# > TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>
# 
# > Alice/Bob:

# 
# > We've got a new employee and three contractors coming in over the next few weeks. This seems like a good time to get all the users and groups in sync across all the machines.

cxos="jan kim sam"
contractors="dani eve fred"
us="alice bob"
other_employees="charlie gert"

# > Kim isn't ready to deploy a directory server yet, so for now, just make sure that they all have accounts on all the machines under your control and that their UID's and GID's are consistent across machines.
# 
# > Please create user-accounts for yourselves, me, Charlie, Dani, Eve, Fred, and Gert.

password=Freedom-16
for user in $contractors $cxos $us $other_employees; do
  id $user > /dev/null &&  userdel -r $user
done
sudo groupdel contractors
sudo groupdel linux_admins
