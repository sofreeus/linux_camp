#!/bin/bash -eux
PATH+=:/sbin:/user/sbin:/usr/local/sbin

# Lab: Create users and groups
# ============================
# 
# ---
# 
# > FROM: Jan <jan@macguffins.test>
# 
# > TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>
# 
# > Alice/Bob:

# 
# > We've got a new employee and three contractors coming in over the next few weeks. This seems like a good time to get all the users and groups in sync across all the machines.

cxos="jan kim sam"
contractors="dani eve fred"
us="alice bob"
other_employees="charlie gert"

# > Kim isn't ready to deploy a directory server yet, so for now, just make sure that they all have accounts on all the machines under your control and that their UID's and GID's are consistent across machines.
# 
# > Please create user-accounts for yourselves, me, Charlie, Dani, Eve, Fred, and Gert.

password=Freedom-16
for user in $contractors $cxos $us $other_employees; do
  id $user > /dev/null ||  useradd $user
  printf "$password\n$password\n" | sudo passwd $user
done

# 
# > We also need two groups: 'contractors' should have Dani, Eve, and Fred,
# > and 'linux_admins' should have you two, me, and Eve.

sudo groupadd contractors

sudo groupadd linux_admins
linux_admins="$us $cxos eve"
for user in $linux_admins; do
  sudo usermod -aG linux_admins $user
done

# 
# > Don't worry! I'm just having her help out with some security work Sam wants done.
# 
# > Make sure all the contractors' accounts expire in six months.

exp_date=$( date -d "+1 day" --iso )
for user in $contractors; do
  sudo usermod -aG contractors -e $exp_date $user
done

# 
# > After your account is on W1, please make sure you can easily send and receive email. It's hugely important around here (as you can see).
# 
# > Thanks,
# 
# > --Jan
# 
# ---
# 
# > FROM: Kim <kim@macguffins.test>
# 
# > TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>
# 
# > Guys,
# 
# > Writing a script to setup the accounts, groups, and expirations, and a matching script to tear them down. Alternate between them until the setup script works perfectly. The distros have different user defaults.
# 
# > Be particularly aware of the User Private Group scheme, home-folders, and preferred shells. I think the useradd switch to create the home-folder is -m. If you've got a distro that doesn't do UPG... I think there's a switch for that, too. I guess you could just ignore it. Jan probably won't notice.
# 
# > HTH, --Kim
# 
# ---
# 
# ### Process Notes
# 
# To convert openSUSE from traditional unix to User Private Group scheme, in `/etc/login.defs`, change umask from 022 to 002 and USERGROUPS_ENAB from no to yes.
# 
# Use "passwd --all (some_user) to see a lot of info at once.
# 
# In real life, make the expiring accounts expire in a day. In the real world you wouldn't, because few contracts are that short, but for training, we want to see how accounts behave when they expire.
# 
# Configure email on W1 in Bob or Alice's account. Confirm that you can send and receive email using a real email account of your choice. If you have a web-mail account, like Hotmail or gmail, you can use that, or if you'd rather not use an existing account, let the instructor know and one will be configured for you on SFS' Zimbra server. If you have time, try a mail user agent (MUA) you've never used before.
# 
# Confirm that Eve is both a contractor and a linux_admin on all the systems with `id eve`. Confirm that each user is in a user-private-group on each system, that the UID's and GID for each user are the same on all systems, and each user has a home-folder on each system. `chage --list eve` on all three systems to confirm that that account expirations are configured correctly.
# 
# 
# ### Commands and configs
# 
# useradd, usermod, groupadd, id, getent passwd, getent group, chage
# 
# /etc/{login.defs,passwd,shadow,group,gshadow}
