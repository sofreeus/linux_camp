#!/bin/bash -eu

PATH+=:/sbin:/usr/sbin:/usr/local/sbin

# Can you send me a report of all the physical storage devices? For each disk, what sort of partition table does the device have? For each partition, give me file-system type, size, % used, and identifiers, like label, serial-number, and UUID.

#Binaries: lsblk, blkid, fdisk, parted, df, pvs, vgs, lvs, pvdisplay, vgdisplay, lvdisplay
#Dev-nodes: /dev/disk/, /dev/[sh]d*
#Configs: /etc/fstab
#Kernel virtual-filesystem: /proc/mounts

# For each disk, what sort of partition table does the device have?

printf 'Disk\t\tPartition-table type\n'
parted -lm | perl -F: -lane 'print "$F[0]\t$F[5]" if m(/dev/[sh]d[\D])'

# For each partition, give me
#   file-system type
#   size
#   %used
#   identifiers
echo
printf 'Partition\tType\tSize\tPercent used\n'
df -hT | perl -lane 's/$F[6]//; print "$F[0]\t$F[1]\t$F[2]\t$F[5]" if m(/dev/[sh].*)'
#     label
#     serial-number
#     uuid


#udevadm info --name /dev/sda1 | grep -i serial_short
#e2label /dev/sda1
