WEB SERVER LAB
https://gitlab.com/sofreeus/linux_camp/blob/master/class/Lab-Web-O.md

S2 will be the web server.
W1 will be the the client (website user).
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Add host entries to /etc/hosts.

  $ vim /etc/hosts

192.168.134.1 S1.MacGuffins.Test RackMaster ClassMaster
192.168.134.2 S2.MacGuffins.Test www.macguffins.test dev.macguffins.test secu
re.macguffins.test macguffins.test

-==[ STUDENT 5 ]=--
192.168.134.51 S1.WalterSobchak.MacGuffins.Test S1
192.168.134.52 S2.WalterSobchak.MacGuffins.Test S2 www.waltersobchak.macguffins.test dev.waltersobchak.macguffins.test secure.waltersobchak.macguffins.test prod.waltersobchak.macguffins.test waltersobchak.macguffins.test
192.168.134.53 S3.WalterSobchak.MacGuffins.Test S3
192.168.134.54 S4.WalterSobchak.MacGuffins.Test S4
192.168.134.55 S5.WalterSobchak.MacGuffins.Test S5
192.168.134.56 S6.WalterSobchak.MacGuffins.Test S6
192.168.134.57 S7.WalterSobchak.MacGuffins.Test S7
192.168.134.58 S8.WalterSobchak.MacGuffins.Test S8
192.168.134.59 W1.WalterSobchak.MacGuffins.Test W1

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Use curl to test each of the websites.
  $ curl waltersobchak.macguffins.test
etc.....
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
To build the web server on Ubuntu, use the tasksel command to install the LAMP bundle.

 $ sudo tasksel

Select the LAMP server and select Ok.
Apache, MySQL and PHP will all be installed and configure.

From S2,
  $ curl localhost
From W1, 
  $ curl s2
You should see the html output from the default Apache page.

We will put all of the websites in /srv on S2.

  $ sudo mkdir /srv/dev
  $ cd /srv
  $ echo "dev site" | sudo tee dev/index.html
  $ cat dev/index.html

Edit the Apache configuration
  $ sudo vim /etc/apache2/apache2.conf

Uncomment the section on <Directory /srv/>...</Directory>
Save file.

Now let's make a new website.
  $ cd /etc/apache2
  $ ll
  $ cd sites-available
  $ ll
  $ cat 000-default.conf | sudo tee dev.conf  (or sudo cp 000-default.conf dev.conf)
  $ sudo vim dev.conf

First, uncomment the ServerName; delete the 7 lines above it.
Here is what your dev.conf file should look like.
<VirtualHost *:80>
   ServerName dev.alicorn.macguffins.test
   DocumentRoot /srv/dev
</VirtualHost>

  $ a2 [tab]
  $ sudo a2ensite dev  (this creates a symlink in /etc/apache2/sites-enabled)

  $ sudo apachectl configtest  (do this before you take your web server down)
  $ sudo service apache2 restart

Now test this in your web browser to make sure that the v-host and the websites are all working correctly.

dev.alicorn.macguffins.test  (http://dev.waltersobchak.macguffins.test/)
www.alicorn.macguffins.test
secure.alicorn.macguffins.test
s2.alicorn.macguffins.test

At this point, only the dev website should be working; the others will display the Apache page.  Let's repeat this process now for the other websites.

------

Let's make a "prod" site on S2.

  $ cd /etc/apache2/sites-available
  $ sudo cp dev.conf prod.conf
Edit the file
  ServerName prod.alicorn....
  ServerAlias www.alicorn.macguffins.test alicorn.macguffins.test
  DocumentRoot /srv/prod

Create and enable the file.
  $ ll
  $ sudo mkdir /srv/prod
  $ echo "prod www domain site" | sudo tee /srv/prod/index.html
  $ sudo a2ensite prod
  $ sudo apachectl configtest
  $ sudo service apache2 restart
Now test in your web browser,
www.alicorn.macguffins.test
alicorn.macguffins.test
prod.alicorn.macguffins.test

------

Security....
This is secure in the way that it will require a login, but there is no encryption so this really is NOT secure.

  $ cd /etc/apache2/sites-available
  $ sudo cp prod.conf secure.conf
  $ sudo vim secure.conf
<VirtualHost *:80>
	ServerName secure.waltersobchak.macguffins.test
	DocumentRoot /srv/secure
</VirtualHost>

  $ sudo mkdir /srv/secure
  $ echo "This is the secure site" | sudo tee /srv/secure/index.html
  $ sudo a2ensite secure
  $ sudo apachectl configtest
  $ sudo service apache2 restart
Now test in web browser

  $ sudo touch /srv/secure/.htaccess

.htaccess file should contain:

AuthType Basic
AuthName "alicorn"
AuthUserFile "/etc/apache2/alicorn.passwords"
AuthGroupFile "/etc/apache2/alicorn.groups"
Require group CXO

The AuthName field should be the name of your user-database.

  $ cd /etc/apache2
  $ sudo vim alicorn-groups
CXO: jan kim sam

  $ sudo htpasswd -c alicorn.passwords jan    (the -c is required on the first file in order to create the .passwords file; don't do it on the others though.)
might need to install apache2-utils
  $ sudo htpasswd alicorn.passwords sam
  $ sudo htpasswd alicorn.passwords kim

In /etc/apache2.conf, in the /srv area, change 
AllowOverride AuthConfig

  $ sudo a2enmod authz_groupfile
  $ sudo apachectl configtest
  $ sudo service apache2 restart

To test from command line:
  $ curl -u kim secure.alicorn.macguffins.test

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
