# build w1 workstation - answer key

**step 0**: type `ip a` command to obtain your IP address (should be next to something like enp0s3 or eth0). Write it down.

Also update packages:
* for centos/redhat/fedora: `sudo yum update`
* for ubuntu: `sudo apt update` and then `sudo apt upgrade`

***
## to change hostname:
* `sudo hostnamectl set-hostname w1.(student_project_name).test`

* `hostname` to check that it really got created

* `cat /etc/hostname` notice this file already contains your new hostname. The hostnamectl command puts it there.

* `reboot` rebooting makes your new hostname show at the shell prompt (ie: [asayre@w1 ~]$)

* NOTE FOR UBUNTU: you need to update the /etc/hosts file with the new hostname right away:
  * `sudo vim /etc/hosts` note: you can use vi or nano instead of vim
  * add (or replace existing) new hostname to this line: 127.0.1.1 or add it to a new line with your new IP address (use command `ip a` to get your IP address)
***

* now save and exit the file by clicking **shift+zz**

## to create new user 'builder'
* for centos/redhat/fedora (UBUNTU USERS SEE BELOW):
  * `sudo useradd builder`
  * `id builder`  to check that it really got created

  * `sudo passwd builder` give your new user a password
    * you will be prompted to enter a new password twice
      * note - for extra credit, how would you set the password at the same time as creating the user? (hint - useradd --help)

        ** OR **
* for ubuntu:
  * `sudo adduser builder` for ubuntu
    * you will automatically be prompted for password

      ** NOW MAKE USER A SUDOER **

* for centos/redhat/fedora:
  * `sudo usermod -aG wheel builder`

* for ubuntu:
  * `sudo usermod -aG sudo builder`

* exit and log back in as builder for changes to take effect
***
## to check/install/enable/start SSH
* for centos/redhat/fedora (UBUNTU USERS SEE BELOW):

  * `sudo systemctl status sshd` check to see if ssh is installed/enabled/running

    * `sudo systemctl enable sshd`, `sudo systemctl start sshd`

  * `sudo yum install openssh openssh-server openssh-clients openssl-libs` if not already installed, install it


* for UBUNTU:

* `sudo systemctl status sshd` check to see if ssh is installed/enabled/running


  * `sudo apt install openssh-server openssh-client` if not already installed, install it
    * should automatically be enabled, active, and running
