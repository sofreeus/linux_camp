# make a server into a webserver

download all packages required

turn on all services

turn off firewall or punch hole in it

make a simple html home page and save it the www folder

***

**(0)** on one of your servers (s1 or s2) download appropriate packages needed to make a webserver and start enable their services

* for centos/rh/fedora - SEE BELOW UBUNTU

  * `yum install httpd`

  * `systemctl status httpd`

  * if its not active/running then: `systemctl start httpd` and `systemctl enable httpd`

    -- OR --

* for UBUNTU

  * `apt install apache2` (for UBUNTU)

  * `systemctl status apache2`

  * if its not active/running then: `systemctl start httpd` and `systemctl enable httpd`


* **NOTE** - once downloaded and started, the service uses the following folders on the server to deliver web content:
  * for centos/rh/fedora, they are:
    * /etc/httpd/ (for configuration files for http service)
    * /var/www/ (for placement of actual html files for web pages)

  * for UBUNTU:
    * /etc/apache2/ (for configuration files for http service)
    * /var/www/ (for placement of actual html files for web pages)

  * navigate to these folders and review their contents: `cd /etc/httpd/` ....


* go back to your workstation and open a browser. In the address field type in the IP address of your webserver.

* you should see the default webpage! The actual file on the server that the browser is displaying is: /var/www/html/index.html


* lets change this file to something custom:
  * go back to your webserver (s1 or s2)
  * move the default page out of the directory: `mv /var/www/html/index.html /var/www/index.html.bk`

  * now make your own: `vim /var/www/html/index.html`

  * a new blank file should open, put whatever contents you want in it:

example:

  ```html

<head>this is my awesome webpage!</head>

<p><b>linux rocks!</b></p>
  ```

* shift+zz to save and exit

* go back to your browser and refresh the page, you may need to reboot your workstation to see the change: `reboot`
