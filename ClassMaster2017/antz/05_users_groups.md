# create users and groups

create accounts for alice, bob, jan, kim, sam, charlie, dani, eve, fred, and gert

manually and also with a script

create groups, put some users in each

set up email for a user

make user accounts expire

***
**0)** on all servers and clients, create users: alice, bob, kim, sam, charlie, dani, eve, alice, and gert

* `useradd -m -s /bin/bash -u 10000 alice`, etc.. for each user. The '-u' switch allows us to specify the uid. the user's private group (gid) will be the same as the specified uid. this is how we will make sure the uids and gids are the same for each user across all systems. for alice I chose 10000. the next user's uid will be 10001, etc... increment each uid by 1. I want my uids to be very high numbers to avoid any risk of accidentally using any preexisting uids.


* `cat /etc/passwd` or `getent passwd alice` to confirm your user is created
* `cat /etc/shadow` or `getent shadow alice` to see more info about your user:

    ```

    alice:!!:17524:0:99999:7::::
    ```
* '!!' means there is no password for this user yet

* `passwd alice`... etc, to make a password for each user
* `/etc/shadow` or `getent shadow alice` again to verify the password command worked:

    ```

    alice:$6$6Y/fI1nx$zQJj6AH9asTNfhxV7NoVgxByJyE.rVKK6tKXiOGNCfWBsrTGY7wtC6Cep6co9eVNkRFrpK6koXs1NU3AZQF8v/:17524:0:99999:7::::
    ```

* now instead of '!!' there is a long encrypted hash value, which means you have a proper secure password for this user

***

**1)** create groups to put the users in, add users to groups
* create one group called admins, the other called contractors:
  * `addgroup -g 2000 admins`
  * `addgroup -g 2001 contractors`

    (the '-g' switch allows us to specify the group id, 2000 and 2001 in this case)
  * `cat /etc/group` or `getent group [group name]` to verify your groups have been created


* add a user to a group:
  * `usermod -aG admins alice`
  * `id alice` to verify alice has been added to the admins group

    NOTE - to remove user from a group: `gpasswd -d alice admins`

      -- OR --

    manually remove user from group in /etc/group and etc/gshadow using vi, vim, nano, or other (should work on all *nix systems)

  * `id alice` to confirm the groups alice has been added to

  ***

  **2)** put expiration on a user password

* password expiration:

  * `chage -M 1 fred` changes the age of expiration for this user's password. the "-M" allows you to enter a numeric value representing number of days from whenever the chage command was entered. in this example, the password will expire in exactly one day. the "-E" switch allows you to enter a specific date in this format: "yyyy-mm-dd"

  * `chage -l fred` to verify the chage command worked



  ***


  * here's a **bash script** to set up all users and another to remove them all (courtesy of David Willson):

* first create a new file and copy below bash script into it: `vim create_users`


  ```

#!/bin/bash -eu

users="alice bob jan kim sam charlie dani eve fred gert"

contractors="dani eve fred"

linux_admins="alice bob jan eve"

uid=10000
for user in $users
do
    echo "Creating $user($uid)..."
    sudo useradd -m -s /bin/bash -u $uid $user
    uid=$(( $uid + 1 ))
done

sudo groupadd contractors

for contractor in $contractors
do
    echo "Adding $contractor to contractors..."
    sudo usermod -aG contractors $contractor
    # account expiry
    echo "Setting $contractor's account to expire..."
    sudo chage -E $( date -d "1 day" --iso ) $contractor
done

sudo groupadd admins

for admin in $admins
do
    echo "Adding $admin to admins..."
    sudo usermod -aG admins $admin
done

```

* once saved, type `ls -l` to see permission levels (no 'x' means this script cannot execute!)

```

-rw-r--r--. 1 root root 728 Jan 9 20:00 create_users
```

* change permissions to make it an executable: `chmod +x create_users`
* type `ls -l` again to see there should now be three 'x' characters in the permissions:

```

-rwxr-xr-x. 1 root root 728 Jan 9 20:00 create_users
```


* now execute it: `./create_users`
* make sure it worked: `getent passwd` and `getent group`
* also verify that each new user has his own home directory: `ls /home/`

* now create script to remove/delete users: `vim remove_users`M
```

#!/bin/bash -eu

users="alice bob jan kim sam charlie dani eve fred gert"

for user in $users
do
    sudo userdel -r $user || echo IDGAF
done

sudo groupdel contractors  || echo IDGAF
sudo groupdel linux_admins || echo IDGAF
```
* `chmod +x remove_users`
* `./remove_users`
* `getent passwd`
* `getent group`



***
### dont do this section!
* **configure e-mail**:
  * install/configure mail transfer agent on workstation (postfix in this case, sendmail also works)

    * ensure postfix is installed and running: `systemctl status postfix`.

    * install it if it is not yet installed: `yum install -y postfix` (centos/rh/fedora) `apt install -y postfix` (UBUNTU)

    * open the main postfix config file in text editor: `vim /etc/postfix/main.cf`

    * insert the following at the bottom of the file:

    ```

    smtp_sasl_auth_enable = yes
    smtp_sasl_mechanism_filter = plain, login
    smtp_sasl_security_options = noanonymous
    smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
    relayhost = [domain.name.of.relay.server]

     # optional:
    smtp_generic_maps = hash:/etc/postfix/generic #if you choose to set up a "FROM" alias
    smtp_tls_security_level = encrypt #if tls is required by email server - like Office365
    inet_protocols = ipv4 #if using Office365 email

    ```

    (sub in the relay server of your choice, example: zimbra.sofree.us)

  * `postfix reload` (updates the postfix process with your changes)

  * create the sasl_passwd file with your email account and password:

      * `vim /etc/postfix/sasl_passwd`:

      ```

      relay.server.name user@relay.server.name:password

      ```

      my example:

      ```

      zimbra.sofree.us asayre@sofree.us:MyPassword

      ```
      * `postmap hash:/etc/postfix/sasl_passwd` (updates the postfix process with your changes)

      * change ownership of file: `chown root: /etc/postfix/sasl_passwd`

      * change permissions of file: `chmod 600 /etc/postfix/sasl_passwd`

      * check that your changes took effect: `ls -l /etc/postfix/sasl_passwd`
        result should be something like:

        ```

        -rw-------. 1 root root 47 Dec 25 23:14 /etc/postfix/sasl_passwd
        ```

  * test your work by sending an e-mail from your workstation:
    * `echo "test e-mail body" | mail -s "test e-mail subject" user@gmail.com`

    ***

      * **if** you would like the relayed email to appear as though it was coming from another email address, not the actual email address of your postfix server user:

        * `vim /etc/postfix/generic`

          create association between the postfix user email address and the new email address (make it a real address like your g-mail, hotmail, or work e-mail address):

          ```

          user@your.postfix.server user@your.gmail.account

          ```

        * `postmap /etc/postfix/generic` (updates the postfix process with your changes)

        * redo your test and see what email address appears in the "FROM" field:
          * `echo "test e-mail body part 2" | mail -s "test e-mail subject part 2" user@gmail.com`
