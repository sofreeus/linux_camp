#!/bin/bash -eu

# log start
logger "A system backup is beginning..."

# Backup S1:/home/
sudo rsync -avHP --delete s1:/home/ /tmp/s1-home/
# Backup S2:/home/
sudo rsync -avHP --delete s2:/home/ /tmp/s2-home/
# Backup /home/
sudo rsync -avHP --delete /home/ /tmp/w1-home/

# Create a timestamp-named archive of the stuphs
sudo tar czvf /tmp/stuph-$( date --iso=min | tr -d : ).tgz /tmp/??-home/

# log completion
logger "A system backup has finished."

