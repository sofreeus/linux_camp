#!/bin/bash -eu
sudo lvremove --force /dev/mapper/*SnappyBackup
thing="/etc/"
device_of_thing="$( df $thing | tail -n 1 | awk '{print $1}' )"
# need some logic here to determine path of thing, relative to device
echo device_of_thing=$device_of_thing
tmpdir=$( mktemp -d )
echo tmpdir=$tmpdir
sudo lvcreate -s $device_of_thing -l 100%FREE --name SnappyBackup
snapshot_device=/dev/mapper/*SnappyBackup
echo snapshot_device=$snapshot_device
#mount
sudo mount $snapshot_device $tmpdir
#work
echo not doing any work
#umount
sudo umount $snapshot_device $tmpdir
sudo lvremove --force /dev/mapper/*SnappyBackup
rmdir $tmpdir
