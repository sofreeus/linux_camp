#!/bin/bash -eu

echo -n "CPU Model: "
lscpu | grep "Model name" | awk -F: '{print $2}' | sed 's/^\ *//'
echo -n "CPU Qty:   "
lscpu | egrep "^CPU\(s\)\:" | awk -F: '{print $2}' | sed 's/^\ *//'

echo -n "Total Mem: "
cat /proc/meminfo | grep MemTotal | awk -F: '{print $2}' | sed 's/^\ *//'

echo    "Network:   "
lspci | grep -i net | cut -c9- | while read line
    do echo "    $line"
done

echo -n "Video:     "
lspci | egrep -i graphic\|vga | cut -c9-

echo    "Disk:      "
df -h | grep -v tmpfs | grep -v udev | while read line
    do echo "    $line"
done

echo    "Activity:  "
top -n 1 -b | head -5 | while read line
    do echo "    $line"
done
