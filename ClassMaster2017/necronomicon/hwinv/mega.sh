#!/bin/bash -eu

me=$(whoami)
case "$me" in
	"root")
		true
		;;
	*)
		echo "Run this script with sudo."
		exit
		;;
esac

echo ""
echo "PCI DEVICES"
echo "==========="
lspci

echo ""
echo "SCSI DEVICES"
echo "============"
lsscsi

echo ""
echo "BLOCK INFORMATION"
echo "================="
lsblk
blkid

echo ""
echo "MEMORY"
echo "======"
free -m
cat /proc/meminfo

echo ""
echo "CPU"
echo "==="
lscpu
cat /proc/cpuinfo

echo ""
echo "Desktop Management Interface"
echo "============================"
dmidecode

echo ""
echo "DISK"
echo "===="
df -h

echo ""
echo "ACTIVITY SNAPSHOT"
echo "================="
top -n 1 -b | head -5
