#!/bin/bash -eu

users="alice bob jan kim sam charlie dani eve fred gert"
contractors="dani eve fred"
linux_admins="alice bob jan eve"

uid=10000

if [ $# -lt 1 ]
then
    echo "Arguments: up - create users, down - delete users"
    exit
fi

if [ "$1" == "up" ]
then
    groupadd contractors
    groupadd linux_admins

    for user in $users
    do
        useradd -m -s /bin/bash -u $uid $user
        uid=$(( $uid + 1 ))
    done
    
    for contractor in $contractors
    do
        chage -E $(date -d "6 months" +"%Y-%m-%d") $contractor
        usermod -aG contractors $contractor
    done
    
    for linux_admin in $linux_admins
    do
        usermod -aG linux_admins $linux_admin
    done

    for user in $users
    do
        id $user
    done
elif [ "$1" == "down" ]
then
    set +e
    for user in $users
    do
        echo "Removing $user"
        userdel -r $user
    done

    echo "Removing group contractors"
    groupdel contractors
    echo "Removing group linux_admins"
    groupdel linux_admins
fi
