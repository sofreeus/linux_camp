#! /bin/bash -eu

G=$( groups $USER )
G=$( echo $G | grep -oh linux_admins )

if [ "$G" == 'linux_admins' ]; then
	export PATH=$PATH:/usr/local/sbin:/usr/sbin/:/sbin/:/commons/gromero.labs/16.scripts/
	echo "Path Updated"
else
	echo "Path not updated"
fi
