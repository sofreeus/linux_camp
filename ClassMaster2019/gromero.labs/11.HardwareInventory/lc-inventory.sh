#! /bin/bash -eu
#
# By Gary Romero
# Linux Camp Grinder Edition
# Machine Inventory Script


echo '--- Processor and Speeds ---'
grep -E 'cpu HMz|bogomips' /proc/cpuinfo | sort | uniq -c
grep -E 'model name' /proc/cpuinfo | sort | uniq -c

echo '--- Total Memory ---'
grep MemTotal /proc/meminfo

echo '--- Storage Info ---'
lsblk

echo '--- Network Info ---'
lspci | grep -E -i 'Ethernet|Wireless'

echo '--- Video Card Info ---'
lspci -nn | grep VGA

echo '--- System Condition ---'
echo -e "\e[31mWhat is the condition of this machine?\e[00m">&2
read condition
case $condition in
*ood)
	echo "System condition is good"
	;;
*ad)
	echo "System condition is bad"
	;;
*gly)
	echo "System condition is ugly"
	;;
*)
	echo "No system condition recieved" && echo $condition
	;;
esac
