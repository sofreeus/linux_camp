#! /bin/bash -eu
#
# By Gary Romero
# 11/9/2019
# linux camp grinder edition

NEWUSERS='gary jan kim sam charlie dani eve fred gert'
NEWGROUPS='contractors linux_admins'

for i in $NEWUSERS; do
	sudo userdel $i
	echo "user $i removed"
done

for i in $NEWGROUPS; do
	sudo groupdel $i
	echo "group $i removed"
done


