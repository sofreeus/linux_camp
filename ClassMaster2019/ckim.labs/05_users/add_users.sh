#!/bin/bash

newusers='alice bob kim charlie eve fred jan dani sam gert'
newgroups='contractors linux_admins'
contractors='dani eve fred'
linux_admins='alice bob jan eve'
u_id='1200'
g_id='1200'

#Check to see if running as root (sudo)
if [ "$EUID" -ne 0 ]
  then echo "Please sudo to run as root"
  exit
fi

echo "New Groups:"
for i in ${newgroups}
do
	groupadd -g ${g_id} ${i}
	g_id=$(( g_id + 1 ))
	echo "Created group ${i}"
done

echo "New users:"
for i in ${newusers}
do
	useradd -m -u ${u_id} ${i}
	usermod --shell /bin/bash ${i}
	u_id=$(( u_id + 1 ))
	echo "Created user ${i}...set shell to /bin/bash"
done

echo "Contractors:"
for i in ${contractors}
do
	usermod -aG contractors ${i}
	chage -E `date -d "180 days" +"%Y-%m-%d"` ${i}
	echo "added: ${i} to contractors"
done

echo "Linix Admins:"
for i in ${linux_admins}
do
	usermod -aG linux_admins ${i}
	echo "added: ${i} to linux_admins"
done

