#!/bin/bash

removeusers='alice bob kim charlie eve fred jan dani sam gert'
removegroups='contractors linux_admins'

#Check to see if running as root (sudo)
if [ "$EUID" -ne 0 ]
  then echo "Please sudo to run as root"
  exit
fi

echo "Remove Groups:"
for i in ${removegroups}
do
	groupdel ${i}
	echo " groupdel ${i}"
done

echo "Remove users:"
for i in ${removeusers}
do
	userdel -r ${i}
	echo "userdel -r ${i}"
done

